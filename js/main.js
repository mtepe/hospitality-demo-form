let selectedPlan = 2
let starRating = 2

const form = $("#wizard").show()

const adjustHeader = (newIndex) => {
    if ( newIndex === 1 ) {
        $('.steps ul').addClass('step-2');
    } else {
        $('.steps ul').removeClass('step-2');
    }
    if ( newIndex === 2 ) {
        $('.steps ul').addClass('step-3');
    } else {
        $('.steps ul').removeClass('step-3');
    }

    if ( newIndex === 3 ) {
        $('.steps ul').addClass('step-4');
        form.addClass('last-step')
        // $('.actions ul').addClass('step-last');
    } else {
        $('.steps ul').removeClass('step-4');
        $('.actions ul').removeClass('step-last');
        form.removeClass('last-step')
    }
}

form.steps({
    headerTag: "h4",
    bodyTag: "section",
    transitionEffect: "fade",
    labels: {
        finish: 'Request Demo'
    },
    onStepChanging: function (event, currentIndex, newIndex) {
        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex) {
            adjustHeader(newIndex)
            return true;
        }
        // Needed in some cases if the user went back (clean up)
        if (currentIndex < newIndex) {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            adjustHeader(newIndex)
        }

        form.validate().settings.ignore = ":disabled,:hidden";

        if (!form.valid()) return false

        // adjustHeader(newIndex)
        return true
    },
    onStepChanged: function (event, currentIndex, priorIndex)
    {
    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (event, currentIndex)
    {
        const formData = new FormData(document.getElementById('wizard'))
        formData.append('plan', selectedPlan)
        formData.append('stars', selectedPlan)
        const data = {}
        for (const [key, value] of formData.entries()) {
            data[key] = value
        }
        console.log(data)
    }
})
.validate({
    rules: {
        first: {
            required: true,
            minlength: 2
        },
        last: {
            required: true,
            minlength: 2
        },
        email: {
            required: true,
            email: true,
        },
        password: {
            required: true,
            minlength: 8,
            strongPwd: true
        },
        passwordConfirm: {
            required: true,
            equalTo: '#password'
        },
        hotelName: {
            required: true,
        },
        hotelCountry: {
            required: true,
        },
        hotelCity: {
            required: true,
        },
        rooms: {
            required: true,
            min: 1,
        },
        taxId: {
            required: true,
        },
        taxType: {
            required: true,
        },
        // ccName: {
        //     required: true
        // },
        // ccNumber: {
        //     required: true
        // },
        // ccExpirationDate: {
        //     required: true
        // },
        // ccSecurityCode: {
        //     required: true
        // },
    },
});

function strongPwd(val) {
    const regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})")
    return regex.test(val)
}

const pwdErr = 'Password must contain at least one uppercase, one lowercase and one symbol character without any whitespaces.'

$.validator.addMethod('strongPwd', strongPwd, pwdErr)

$(document).on('click', '.choose-plan', function (event) {
    selectedPlan = $(this).data('value')
    $('.choose-plan').removeClass('selected').text('Select Plan')
    $(this).addClass('selected').text('Selected')
})

const positionLogo = () => {
    const w = $('#wizard').width()
    const imageW = $('#tivicon-logo').width()
    $('#tivicon-logo').css('left', ((w / 2) - (imageW / 2)))
}

positionLogo()
window.addEventListener('resize', positionLogo)