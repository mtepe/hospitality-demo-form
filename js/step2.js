const countriesSelect = document.getElementById('countries')
const citiesSelect = document.getElementById('cities')
const regionsSelect = document.getElementById('regions')

const updateCity = () => {
    const currentCountryCities = window.allCountries[countriesSelect.value]
    citiesSelect.innerHTML = ''
    // regionsSelect.innerHTML = ''
    for (const city of currentCountryCities) {
        const option = document.createElement('option')
        option.innerText = city
        option.value = city
        citiesSelect.appendChild(option)
    }
}

const getCountries = () => {
    for (const countryKey in window.allCountries) {
        const option = document.createElement('option')
        option.innerText = countryKey
        option.value = countryKey
        countriesSelect.appendChild(option)
    }

    updateCity()
    $('#countries').select2({
        width: '100%',
    });

    $('#cities').select2({
        width: '100%',
    });
    $('#taxType').select2({
        width: '100%',
    });
}

getCountries()

$('#countries').on('select2:select', updateCity)

